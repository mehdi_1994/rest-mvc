<?php
use App\Core\Route;
Route::Get('/products','ProductController@Index');
//Route::Post('/products/store','ProductController@Store');
Route::Get('/products/store','ProductController@Store');
Route::Get('/products/delete','ProductController@Delete');
Route::Post('/products/edit','ProductController@Edit');
Route::Get('/products/showform','ProductController@ShowForm',true);
Route::Get('/products/category','ProductController@Category');
Route::Post('/products/upload','ProductController@Upload');
