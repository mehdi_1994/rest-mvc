<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="../../Assets/css/bootstrap.css" rel="stylesheet">
  <link href="../../Assets/css/bootstrap-icons.css" rel="stylesheet">


  <link rel="icon" href="avatar.jpg">
  <link rel="stylesheet" href="../Assets/css/style.css">
  <title>Document</title>
</head>

<body>

  <div class="container-flouid table-responsive">
    <row>
      <div class="col-12">
        <table class="table table-striped ">
          <thead>
            <tr>
              <th scope="col">ردیف</th>
              <th scope="col">لیست دسته بندی</th>
              <th scope="col">نام محصول</th>
              <th scope="col">آدرس</th>
              <th scope="col">توضیحات</th>
              <th scope="col">تصویر</th>
              <th scope="col">ویرایش</th>
              <th scope="col">افزودن</th>
              <th scope="col">حذف</فا>
            </tr>
          </thead>
          <tbody>

            <?php

            $data = CreateArrFrotPrint($data);
            foreach ($data as $key => $val) :
            ?>
              <tr class="row-product">
                <th data-id="<?= $val['id'] ?>" scope="row"><?= $val['id'] ?></th>
                <td class="category"><?= $val['CATEGORY_TITLE'] ?></td>
                <td class="title"><input readonly type="text" value="<?= $val['title'] ?>"></td>
                <td class="address"><input readonly type="text" value="<?= $val['address'] ?>"></td>
                <td class="description"><input readonly type="text" value="<?= $val['description'] ?>"></td>
                <td class="img"><img src="<?= $val['img'] ?>"></td>
                <td class="edit"><span class="edit-icon"></span></td>
                <td class="add"><span class="add-icon"></span></td>
                <td class="delete"><span></span></td>
              </tr>
            <?php endforeach; ?>

          </tbody>
        </table>
      </div>
    </row>
  </div>
  <p id="p1"></p>
  <p id="p2"></p>
  <p id="p3"></p>
  <script src="../Assets/js/jquery-3.6.0.min.js"></script>
  <script src="../Assets/js/jquery.js"></script>
  <script>



  </script>







</body>

</html>

<script>
  $result = [];
  $result_str = '';

  function add_array_item(array, item) {
    array.push(item);
    return array;
  }

  function remove_array_item(array, item) {
    var index = array.indexOf(item);

    if (index > -1) {
      array.splice(index, 1);
    }

    return array;
  }

  //********************** */
  $('.edit').click(function() {

    var $this = $(this);
    var $span = $this.find('span');

    $result = [];
    $result_str = '';
    var $this = $(this);
    //----------------------------
    $pid = $this.parent().find('th').attr('data-id'),
      $title = $this.parents('.row-product').find('.title input').val();
    $address = $this.parents('.row-product').find('.address input').val();
    $description = $this.parents('.row-product').find('.description input').val();
    $category = $this.parents('.row-product').find('.category').text();
    $category = $category.split('-');
    if ($span.hasClass('edit-icon')) {


      $.get('/products/category', [], function(response) {

        $cats = response['data'];

        var $cat_selected = [];
        var $window = '<div class="mask"><div class="window"><span class="close-window"></span><div class="parent-item"><div class="row"><div class="col-lg-1 col-md-1 col-sm-1 col-1"></div><div class="col-lg-5 col-md-5 col-sm-12 col-12"><div class="row"><div class="col-12"><label class="p-2" for="">نام محصول</label></div><div class="col-12"><label class="p-2" for="">انتخاب دسته بندی</label></div><div class="col-12"><label class="p-2" for="">آدرس</label><div class="col-12"><label class="p-2" for="">توضیحات</label></div></div><div class="col-12"><label class="p-2" for="">انتخاب تصویر</label></div></div></div><div class="col-lg-5 col-md-5 col-sm-12 col-12"><div class="col-12"><input  value="' + $title + '" class="new-title form-control" type="text"></div><div class="col-12 d-flex justify-content-left align-items-center flex-wrap gap-1">' + create($category, $cats) + '</div><div class="col-12"><input class="form-control new-address" value="' + $address + '" type="text"></div><div class="col-12"><input  value="' + $description + '" class="form-control new-desc" type="textarea"></div><div class="col-12"><input class="form-control" type="file"></div><div class="col-12"><button class="submit-form btn btn-success w-100">اضافه</button></div></div><div class="col-lg-1 col-md-1 col-sm-1 col-1"></div></div></div></div></div>';
        $('body').append($window);
        $('.close-window').click(function() {
          $(this).parents('.mask').remove();
        });
        $("input[type=checkbox]").click(function() {
          var $this = $(this);

          if (($this.prop("checked")) == true) {
            add_array_item($cat_selected, $this.attr('data-id'));
          } else {
            remove_array_item($cat_selected, $this.attr('data-id'));
          }
          console.log($cat_selected);
        });
        $('.submit-form').click(function() {
          var $spinner = '<div class="spinner-border" role="status"></div>';
          $('.submit-form').html($spinner);
          $path_img = $('.window').find('input[type=file]').val();
          $desc = $('.new-desc').val();
          $address = $('.new-address').val();
          $title = $('.new-title').val();
          $data = {
            'title': $title,
            'description': $desc,
            'address': $address,
            'img': $path_img,
            'category': $cat_selected,
          };

          console.log(JSON.stringify($cat_selected));
          $cat_selected = JSON.stringify($cat_selected);
          $cat_selected = $cat_selected.replace('[', '');
          $cat_selected = $cat_selected.replace(']', '');
          $cat_selected = $cat_selected.replaceAll('"', '');


          $.post('/products/edit?pid=' + $pid, $data, function(response2) {


            $('.submit-form').html('ارسال');
            if (response2['http_message'] == 'OK') {
              $this.parent().find('.title input').val($data.title);
              $this.parent().find('.address input').val($data.address);
              $this.parent().find('.description input').val($data.description);
              $this.parent().find('.img img').attr('src', $data.img);

              $('body').find('.mask').remove();

            }

          });
        });

      });

    }

    //----------------------------

  });

  function create($category, $cats) {

    $tmp = [];
    $.each($cats, function($key, $val) {

      $.each($category, function($k, $v) {


        if ($val['category_title'] == $v) {
          add_array_item($result, $val['category_title']);
          add_array_item($tmp, $val['id']);
          $result_str += $v + '<input data-id="' + $val['id'] + '" checked class="new-cat" type="checkbox">';
        }

      });
      if ($.inArray($val['category_title'], $result) == -1) {
        add_array_item($result, $val['category_title']);
        $result_str += $val['category_title'] + '<input data-id="' + $val['id'] + '" class="new-cat" type="checkbox">';

      }
    });
    return [$result_str, $tmp];
  }

  function removeDuplicate($marge_array) {
    var uniqueNames = [];
    $.each($marge_array, function(i, el) {
      if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
    });
    return uniqueNames;
  }
  $('.add').click(function() {
    $result = [];
    $result_str = '';
    var $this = $(this);
    //----------------------------
    $title = $this.parents('.row-product').find('.title input').val();
    $address = $this.parents('.row-product').find('.address input').val();
    $description = $this.parents('.row-product').find('.description input').val();
    $category = $this.parents('.row-product').find('.category').text();
    $category = $category.split('-');

    //----------------------------
    $.get('/products/category', [], function(response) {

      $cats = response['data'];

      var $cat_selected = [];
      var $window = '<div class="mask"><div class="window"><span class="close-window"></span><div class="parent-item"><div class="row"><div class="col-lg-1 col-md-1 col-sm-1 col-1"></div><div class="col-lg-5 col-md-5 col-sm-12 col-12"><div class="row"><div class="col-12"><label class="p-2" for="">نام محصول</label></div><div class="col-12"><label class="p-2" for="">انتخاب دسته بندی</label></div><div class="col-12"><label class="p-2" for="">آدرس</label><div class="col-12"><label class="p-2" for="">توضیحات</label></div></div><div class="col-12"><label class="p-2" for="">انتخاب تصویر</label></div></div></div><div class="col-lg-5 col-md-5 col-sm-12 col-12"><div class="col-12"><input  value="' + $title + '" class="new-title form-control" type="text"></div><div class="col-12 d-flex justify-content-left align-items-center flex-wrap gap-1">' + create($category, $cats)[0] + '</div><div class="col-12"><input class="form-control new-address" value="' + $address + '" type="text"></div><div class="col-12"><input  value="' + $description + '" class="form-control new-desc" type="textarea"></div><div class="col-12"><form id="image-upload-form" action="/products/upload"  method="post" enctype="multipart/form-data"><input class="form-control" type="file"><button class="btn btn-primary" id="upload">آپلود</button></form></div><div class="col-12"><button class="submit-form btn btn-success w-100">اضافه</button></div></div><div class="col-lg-1 col-md-1 col-sm-1 col-1"></div></div></div></div></div>';
      $('body').append($window);
      $('.close-window').click(function() {
        $(this).parents('.mask').remove();
      });
      $("input[type=checkbox]").click(function() {
        var $this = $(this);

        if (($this.prop("checked")) == true) {
          add_array_item($cat_selected, $this.attr('data-id'));
        } else {
          remove_array_item($cat_selected, $this.attr('data-id'));
        }

      });

      /**************************** */


      $("#upload").on('click', (function(e) {

        e.preventDefault();
        $form = document.getElementById("image-upload-form");
        console.log($form);
        $.ajax({
          url: "/products/upload",
          type: "POST",
          data: new FormData($form),
          contentType: false,
          cache: false,
          processData: false,
          success: function(data) {
            console.log(data);
          },
          error: function(data) {
            console.log("error");
            console.log(data);
          }
        });

      }));
      //******************* */

      $('.submit-form').click(function() {
        /**************************** */
        $concat = $cat_selected.concat(create($category, $cats)[1]);
        removeDuplicate($concat);
        console.log(removeDuplicate($concat));


        $path_img = $('.window').find('input[type=file]').val();
        $desc = $('.new-desc').val();
        $address = $('.new-address').val();
        $title = $('.new-title').val();
        $data = {
          'title': $title,
          'address': $address,
          'description': $desc,
          'img': $path_img
        };
        console.log($data);

        $.get('/products/store', $data, function(response) {
          console.log(response);
          var $tr = '<tr class="row-product"><th data-id="" scope="row"></th><td class="category"></td><td class="title"><input readonly type="text" value=""></td><td class="address"><input readonly type="text" value=""></td><td class="description"><input readonly type="text" value=""></td><td class="img"><img src=""></td><td class="edit"><span class="edit-icon"></span></td><td class="add"><span class="add-icon"></span></td><td class="delete"><span></span></td></tr>';

        });

      });
    });


    $('.delete').click(function() {
      var $this = $(this);
      var $pid = $this.parent().find('th').attr('data-id');
      if ($('tbody tr').length == 1) return false;
      $.get('/products/delete?' + 'pid=' + $pid, [], function(response) {
        $this.parent().remove();
      });
    });



  });
</script>