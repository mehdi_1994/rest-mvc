<?php

function CreateObjControllerAndRunFn($action,$URL_CONTROLLER){
      
      $action = explode('@',$action);
      $classname = $URL_CONTROLLER . $action[0];
     
    
      $method_controller = $action[1];
    
      $controller = new $classname;
     
      $result = $controller->$method_controller();
      return $result;
}

function Create_Format_Params($params)
{
    unset($params['path']);
    $values = array_values($params);
    $values = array_filter($values);
    $keys = array_keys($params);
    return [implode("','",$values),implode(',',$keys)];
}

function Update_Format_Params($params)
{
    unset($params['path']);
    unset($params['pid']);
    $x = '';
    $counter = 0;
    foreach($params as $key => $val){
  
        if( $counter < (count($params) - 1)){
            $x .= "$key = '$val',";
        }else{
            $x .= "$key = '$val'";
        }
        $counter++;
    }
    return $x;
}


function view($path,$data=[])
{   
    extract($data);
    $path = str_replace('.','/',$path);
    $full_path = BASE_PATH . "/Views/$path.php";
    include $full_path; 
}

function removeEmptyKey($params){
    foreach($params as $key => $val):
        if($params[$key] == ''){unset($params[$key]);}
    endforeach;
    return $params;
}


function CreateArrFrotPrint($data)
{
    $tmp = array();
    foreach($data as $key => $val):  
      if (!array_key_exists($val['id'],$tmp)){
        $tmp[$val['id']] = $val;
      }else{
       $tmp[$val['id']]['CATEGORY_TITLE'] .= '-' . $val['CATEGORY_TITLE'];
      }
    endforeach;
    return $tmp;
}

