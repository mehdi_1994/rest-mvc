<?php

namespace App\Models;

class ProductModel extends Model
{

    public function Category()
    {
        $sql = "SELECT * FROM CATEGORY";
        $result = $this->runQuerySelect($sql, [],'FETCHALL');
        return $result;  
    }


    public function Index($pid)
    {
        $fetch = 'FETCHALL';
        $data = [];
        $sql = 'SELECT PRODUCTS.*,CATEGORY.CATEGORY_TITLE  FROM (PRODUCTS iNNER JOIN PRODUCT_CATEGORY ON PRODUCTS.ID = PRODUCT_CATEGORY.PRODUCT_ID) INNER JOIN CATEGORY ON PRODUCT_CATEGORY.CATEGORY_ID = CATEGORY.ID';
      

        if (isset($pid)) {
            $sql = "SELECT * FROM PRODUCT WHERE id = ?; ";
            $fetch = 'FETCH';
            $data = [$pid];
        }
        $result = $this->runQuerySelect($sql, $data, $fetch);

        
        return $result;
    }
   

    public function Store($params)
    {

        $title = (isset($params['title'])) ? $params['title'] : null;
        $description = (isset($params['description'])) ? $params['description'] : null;
        $address = (isset($params['address'])) ? $params['address'] : null;
        $img = (isset($params['img'])) ? $params['img'] : null;
        if ($title != null) {
            $params = removeEmptyKey($params);
            $params = Create_Format_Params($params);
          //  return $params;
            $sql = "INSERT INTO PRODUCTS ($params[1]) VALUES ('$params[0]')";
            return $sql;
            $result = $this->runQuery($sql);
            return $result;
        }
    }

    public function Delete($params)
    {
        $status = false;
        $pid = (isset($params['pid'])) ? $params['pid'] : null;
        if ($pid != null) {
            $sql = "DELETE FROM PRODUCTS WHERE id = $pid";
            $result = $this->runQuery($sql);
            $status = true;
        }
        return $status;
    }

    public function Edit($params)
    {
       
        $status = false;
        $title = (isset($params['title'])) ? $params['title'] : null;
        $description = (isset($params['description'])) ? $params['description'] : null;
        $address = (isset($params['address'])) ? $params['address'] : null;
        $category = (isset($params['category'])) ? $params['category'] : null;
        $img = (isset($params['img'])) ? $params['img'] : null;
        $pid = (isset($params['pid'])) ? $params['pid'] : null;
        if ($title != null) {
            $upParams = Update_Format_Params($params);
            $sql = "UPDATE PRODUCTS SET $upParams  WHERE id = $pid";
            $result = $this->runQuery($sql);
            $status = true;
        }
        return $status;
    }
}
