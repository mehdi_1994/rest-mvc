<?php
namespace App\Models;
use PDO;
use Exception;

class Model
{
    private $servername = 'localhost';
    private $username = 'root';
    private $password = '';
    private $dbname = 'main';
    public  $connect;
  
   

    public function __construct()
    {
        try
        {
           $this->connect = new PDO("mysql:host=$this->servername;dbname=$this->dbname",$this->username,$this->password);    
          
        }
        catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function runQuery($sql,$data=[])
    {
        $st = $this->connect->prepare($sql);
        foreach($data as $key => $val)
        {
            $st->bindValue($key+1,$val);
        }
        $st->execute();
    }
    public function runQuerySelect($sql,$data=[],$fetch='FETCH')
    {
       
        $st = $this->connect->prepare($sql);
        foreach($data as $key => $val)
        {
            $st->bindValue($key+1,$val);
        }
        $st->execute();
        if($fetch == 'FETCH'){
            $result = $st->fetch(PDO::FETCH_ASSOC);
        }else{

            $result = $st->fetchAll(PDO::FETCH_ASSOC);
         }
            return $result;
          
    }


  
}
