<?php
namespace App\Core;
use App\Core\Route;
use App\Core\Responce;
use App\Core\Request;

class Router
{
    private $request;
    private $routes;
    private $current_route;
    public static $instance;
    const URL_CONTROLLER =  'App\Controllers\\';
    

    public function __construct()
    { 
        $this->request = new Request();   
        $this->routes = Route::AllRoutes();
        $this->current_route = $this->find_route($this->request) ?? null;
    }
  

    public function find_route(Request $request)
    {
        foreach ($this->routes as $key => $route)
        {
            if(($route['uri'] == $request->Uri()) and ($route['method'] == $request->method())){
                
                return $route;
            }  
        }
    }

    public function run()
    {
       $action = $this->current_route['action'] ?? null;
       $uri = $this->current_route['uri'] ?? null;
       $method = $this->current_route['method'] ?? null;
       $tpl = $this->current_route['tpl'] ?? false;



       switch($method){
            case 'get':
            $result = CreateObjControllerAndRunFn($action,self::URL_CONTROLLER);
            ($tpl != true)?Responce::Respond($result,Responce::HTTP_OK):'';
            break;
            case 'post':
            $result = CreateObjControllerAndRunFn($action,self::URL_CONTROLLER); 
                Responce::Respond($result,Responce::HTTP_OK);
            break;
       
            default:
            Responce::Respond([],Responce::HTTP_METHOD_NOT_ALLOWED);
         }    

       
 
    }
    public static function RunInstance()
    {
        (!self::$instance)? self::$instance = new self :self::$instance;
        return self::$instance;
    }

   
    
}