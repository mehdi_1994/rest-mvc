<?php
namespace App\Core;
class Route
{
   public static $Routes;
   
   public static function Add($method,$uri,$action=null,$tpl=false){
    self::$Routes[] = ['method'=>$method,'uri'=>$uri,'action'=>$action,'tpl'=>$tpl];
   }
   public static function Get($uri,$action=null,$tpl=false){self::Add('get',$uri,$action,$tpl);}
   public static function Post($uri,$action=null,$tpl=false){self::Add('post',$uri,$action,$tpl);}
   public static function Put($uri,$action=null,$tpl=false){self::Add('post',$uri,$action,$tpl);}
   public static function AllRoutes(){return self::$Routes;}

}