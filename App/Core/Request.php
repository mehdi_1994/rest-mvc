<?php
namespace App\Core;

class Request
{
    private $params;
    private $method;
    private $uri;
    private $ip;
    public static $instance;
   

    
    public function __construct(){
     
        $this->params = $_REQUEST;
        $this->method = strtolower($_SERVER['REQUEST_METHOD']);
        $this->uri = strtok($_SERVER['REQUEST_URI'],'?');
        $this->ip = $_SERVER['SERVER_ADDR'];
    }

    public function Params()
    {
        return $this->params;
    }

    public function method()
    {
        return $this->method;
    }

    public function Uri()
    {
        return $this->uri;
    }

    public function Ip()
    {
        return $this->ip;
    }
    public static function RunInstance()
    {
        (!self::$instance)? self::$instance = new self :self::$instance;
        return self::$instance;
    }

  

}