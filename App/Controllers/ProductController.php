<?php

namespace App\Controllers;

use App\Core\Request;
use App\Models\ProductModel;

class ProductController
{
    private $request;
    private $params;
    private $productmodel;


    public function __construct()
    {
        $this->request = Request::RunInstance();
        $this->params = $this->request->Params();
        $this->productmodel = new ProductModel;
    }


    public function Index()
    {
        
        $pid = $this->params['pid'] ?? null;
        $result = $this->productmodel->index($pid);
        return $result;
        
        
    }

    public function ShowForm()
    {
        $pid = $this->params['pid'] ?? null;       
        $result = $this->productmodel->index($pid);
        $pid ? $data['data'] = [$result]  : $data = ['data'=> $result];
        view('index.index',$data);
    }

    public function Store()
    {
     
      //  $result = $this->productmodel->Store($this->params); 
        return $this->params; 
    }


    public function Delete()
    {
        $result = $this->productmodel->Delete($this->params);
        return $result;
    }

    public function Edit()
    {
        $result = $this->productmodel->Edit($this->params);
        return  $result;
    }
    public function Category()
    {
        $result = $this->productmodel->Category($this->params);
        return $result;
    }
    public function Upload()
    {
        return  $_FILES;
    }
}
